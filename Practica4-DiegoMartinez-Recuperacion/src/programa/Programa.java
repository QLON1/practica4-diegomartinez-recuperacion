package programa;
import java.util.Scanner;
public class Programa {
		public static void main(String args[]) {
			Scanner input = new Scanner(System.in);

		        System.out.println("Creo La tienda");
		        int maxTiendas=4;
		        clases.Tienda miTienda = new clases.Tienda(maxTiendas);
		        
		        miTienda.altaPastel("pastel de limon", "Limon", 36.00, 17,18);
		        miTienda.altaPastel("pastel de frambuesa", "frambuesa", 22.00, 10, 16);
		        miTienda.altaPastel("pastel de chocolate", "chocolate", 10.00, 6,8);
		        miTienda.altaPastel("pastel de Queso", "Queso", 28.00, 3, 12);
		        miTienda.altaPastel("pastel de Vainilla", "vainilla", 16.00, 14,6);
		        miTienda.altaPastel("pastel de oreo ", "oreo", 16.00, 4, 20);
		        miTienda.altaPastel("pastel de manzana", "manzana", 19.00, 1,10);
		        miTienda.altaPastel("pastel de milka royal ", "milka", 14.00, 20, 13);
		        
		        System.out.println("Listado de todos los pasteles");
		        miTienda.listadopasteles();
		        
		        System.out.println("Buscar un pastel");
		        System.out.println("introduce el nombre del pastel que quieras buscar");
		        String buscarNombre = input.nextLine();
		        System.out.println(miTienda.buscarpasteles(buscarNombre));
		        
		        System.out.println("Listar Pasteles por  Precio");
		        miTienda.listarPastelesPorPrecio("20");
		        
		        System.out.println("Listado de pasteles por sabor");
		        System.out.println("introduce el sabor del pastel que quieras buscar");
		        String listadosabor = input.nextLine();
		        miTienda.listarPastelesPorSabor(listadosabor);
		        
		        System.out.println("Cambiar nombre del pastel");
		        miTienda.cambiarNombre("pastel de manzana", "pastel de manzana caramelizada");
		        miTienda.listadopasteles();
		        
		        System.out.println("Listado de  pasteles por una cantidad de porciones inferior a 15");
		        miTienda.listarPastelesPorPorciones("15");
		        
		        System.out.println("Cambiar sabor del pastel");
		        miTienda.cambiarSabor("manzana", "manzana caramelizada");
		        miTienda.listadopasteles();
		        
		        System.out.println("Eliminar un Pastel");
		        miTienda.eliminarPastel("Pastel de oreo");
		        miTienda.listadopasteles();
		        
		        System.out.println("a�adir un pastel nuevo");
		        miTienda.altaPastel("pastel de kit kat", "kit kat", 11.00, 55, 4);
		        miTienda.listadopasteles();
		        
		        System.out.println("Listado de  pasteles por la cantidad de tartas 10 o superior");
		        miTienda.listarPastelesPorCantidatartas("10");
		        
		        
		        
		        input.close();
	}

}
