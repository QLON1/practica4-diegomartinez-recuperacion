package clases;

public class Tienda {

	 Pastel[] pasteles;
	    public Tienda(int maxpasteles) {
	        this.pasteles = new Pastel[maxpasteles];

	}
//metodo apra dar de alta los pasteles	    
public void altaPastel(String nombre, String sabor, double precio, int cantidatartas, int porciones) {
	for (int i=0; i < pasteles.length; i++) {
		if( pasteles[i] == null) {
			pasteles[i]= new Pastel(nombre, sabor, precio, cantidatartas, porciones);
			break;
		}
	}
}
//metodo para Listar los pasteles
public void listadopasteles() {
    for (int i=0;i<pasteles.length;i++) {
        if (pasteles[i]!=null) {
            System.out.println(pasteles[i]);
        }
    }

}
//metodo para buscar el pastel que queramos
public Pastel buscarpasteles(String nombre) {
	 for (int i=0;i<pasteles.length;i++) {
	     if (pasteles[i]!=null) {
	         if (pasteles[i].getNombre().equals(nombre)) {
	             return pasteles[i];
	         }
	     }
	 }

	 return null;
}
//Metodo para eliminar un pastel
public void eliminarPastel(String nombre) {
    for (int i=0;i<pasteles.length;i++) {
        if (pasteles[i]!=null) {
            if (pasteles[i].getNombre().equals(nombre)) {
                pasteles[i]=null;
            }
        }
    }
}
//metodo por el cual listamos los pasteles con una cantidad de porciones inferior a 15
public void listarPastelesPorPorciones(String porciones) {
    for (int i = 0; i < pasteles.length; i++) {
        if (pasteles[i] !=null) {
            if (pasteles[i].getPorciones()<15) {
                System.out.println(pasteles[i]);

            }
        }
    }
}
//metodo para cambair el nombre de un pastel 
public void cambiarNombre(String nombre, String nombre2) {
    for (int i = 0; i < pasteles.length; i++) {
        if (pasteles[i].getNombre().equals(nombre)) {
            pasteles[i].setNombre(nombre2);
        }
    }
}
//metodo para listar los pasteles con un precio superior a 20
public void listarPastelesPorPrecio(String precio) {
    for (int i = 0; i < pasteles.length; i++) {
        if (pasteles[i] !=null) {
            if (pasteles[i].getPrecio()>20.00) {
                System.out.println(pasteles[i]);

            }
        }
    }
}
//metodo para cambiar el sabor del pastel
public void cambiarSabor(String sabor, String sabor1) {
    for (int i = 0; i < pasteles.length; i++) {
        if (pasteles[i].getSabor().equals(sabor)) {
            pasteles[i].setSabor(sabor1);
        }
    }
}
//metodo para listar los pasteles por sabor
public void listarPastelesPorSabor(String sabor) {
	for (int i=0;i<pasteles.length;i++) {
		if (pasteles[i]!=null) {
			if (pasteles[i].getSabor().equals(sabor)) {
				System.out.println(pasteles[i]);
         }
     }
 }
}
//metodo por el cual  podemos listar  la cantidad de pasteles mayor o igual a 10
public void listarPastelesPorCantidatartas(String cantidatartas) {
    for (int i = 0; i < pasteles.length; i++) {
        if (pasteles[i] !=null) {
            if (pasteles[i].getCantidatartas()>=10) {
                System.out.println(pasteles[i]);

            }
        }
    }
}

}
