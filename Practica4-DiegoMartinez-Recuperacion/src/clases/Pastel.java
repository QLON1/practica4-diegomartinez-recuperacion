package clases;

public class Pastel {
String nombre;
String sabor;
double precio;
int cantidatartas;
int porciones;


public Pastel(String nombre, String sabor, double precio, int cantidatartas, int porciones) {
	super();
	this.nombre = nombre;
	this.sabor = sabor;
	this.precio = precio;
	this.cantidatartas = cantidatartas;
	this.porciones= porciones;
}

public Pastel() {
	super();
	this.nombre = "";
	this.sabor = "";
	this.precio = 0;
	this.cantidatartas = 0;
	this.porciones= 0;
}

public String getNombre() {
	return nombre;
}

public void setNombre(String nombre) {
	this.nombre = nombre;
}

public String getSabor() {
	return sabor;
}

public void setSabor(String sabor) {
	this.sabor = sabor;
}

public double getPrecio() {
	return precio;
}

public void setPrecio(double precio) {
	this.precio = precio;
}

public int getCantidatartas() {
	return cantidatartas;
}

public void setCantidatartas(int cantidatartas) {
	this.cantidatartas = cantidatartas;
}

public int getPorciones() {
	return porciones;
}

public void setPorciones(int porciones) {
	this.porciones = porciones;
}

@Override
public String toString() {
	return "Pastel [nombre=" + nombre + ", sabor=" + sabor + ", precio=" + precio + ", cantidatartas=" + cantidatartas
			+ ", porciones=" + porciones + "]";
}


}
